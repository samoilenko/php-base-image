# Docker, base php image
 
#Push image to the docker hub

 - docker build .  -t php_base:latest
 - docker tag php_base:latest samoilenko/php_base:latest
 - docker push samoilenko/php_base:latest
 
Link to [Push images](https://docs.docker.com/docker-cloud/builds/push-images/)
