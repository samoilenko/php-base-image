FROM php:fpm-alpine

COPY conf/php.ini $PHP_INI_DIR/
COPY install-composer.sh /tmp

RUN apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev libmcrypt-dev autoconf gcc make g++ zlib-dev libzip-dev oniguruma-dev libxslt-dev \
    && chmod +x /tmp/install-composer.sh && sh /tmp/install-composer.sh \
	&& pecl channel-update pecl.php.net \
	&& pecl install mcrypt-1.0.3 \
    && docker-php-ext-configure gd --with-jpeg=/usr \
    && docker-php-ext-install -j$(nproc) iconv zip pdo_mysql gd zip mbstring pdo xsl \
    && pecl install apcu && echo extension=apcu.so > /usr/local/etc/php/conf.d/apcu.ini \
    && docker-php-ext-install opcache \
    && docker-php-ext-enable opcache \
    && rm -rf /var/cache/apk/* && \
           rm -rf /tmp/* \
    && apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev libjpeg-turbo-dev \
    && { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > $PHP_INI_DIR/conf.d/opcache-recommended.ini

WORKDIR "/project"

CMD ["php-fpm"]
